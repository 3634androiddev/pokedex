package com.example.dooba.pokedex;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;

import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.kosalgeek.android.json.JsonConverter;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class PokemonProfile extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    ArrayList<PokeEvolution> pokeEvolves;

    ImageView ivImg;
    ImageView ivType1;
    ImageView ivType2;
    TextView Name;
    ListView lv;
    RecyclerView rvItem1;
    private String[] lv_arr = {};

    BarChart barChart;
    DrawerLayout mDrawerLayout;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    TextToSpeech t1;
    Button b1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_profile);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout3);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(false);

        final Context context = getApplicationContext();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout3);
        NavigationView n = (NavigationView) findViewById(R.id.navigationView3);
        n.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {


                }
                mDrawerLayout.closeDrawers();  // CLOSE DRAWER
                return true;
            }
        });


        ivImg = (ImageView) findViewById(R.id.pokeDP);
        Name = (TextView) findViewById(R.id.name);
        ivType1 = (ImageView) findViewById(R.id.type1);
        ivType2 = (ImageView) findViewById(R.id.type2);
        lv = (ListView) findViewById(R.id.list);
        rvItem1 = (RecyclerView) findViewById(R.id.my_recInner);
        rvItem1.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(this);


        rvItem1.setLayoutManager(manager);

        if (getIntent().getSerializableExtra("pokemon") != null) {
            final Pokemon pokemon = (Pokemon) getIntent().getSerializableExtra("pokemon");
            final int DexID = (int) pokemon.DexID;


            //Set the name
            Name.setText(pokemon.Name);

            //typecasting Pokemon
            String type = pokemon.Type;
            type = type.replaceAll("[^\\x00-\\x7f]", "");
            String deliminator = "/";

            String type1 = "";
            String type2 = "";

            if (type.toLowerCase().contains(deliminator)) {
                String[] splited = type.split("/");
                type1 = splited[0];
                type2 = splited[1];
            } else {
                type1 = type;
                type2 = "none";
            }

            type1.toLowerCase();
            type2.toLowerCase();
            System.out.println(type1);


            //Images

            Uri urii = Uri.parse("android.resource://com.example.dooba.pokedex/drawable/imgbig"+pokemon.DexID);

            Picasso.with(getApplicationContext()).load(urii).into(ivImg);

            ivImg.setFocusable(true);
            ivImg.setFocusableInTouchMode(true);

            //type images
            Uri uri = Uri.parse("android.resource://com.example.dooba.pokedex/drawable/"+type1);
            Picasso.with(getApplicationContext()).load(uri).into(ivType1);
            if (type2 != "none") {
                Uri uri2 = Uri.parse("android.resource://com.example.dooba.pokedex/drawable/"+type2);
                Picasso.with(getApplicationContext()).load(uri2).into(ivType2);
            }


            //Set and show stats
            final Float baseHp = Float.parseFloat(pokemon.BaseHp);
            final Float baseAtk = Float.parseFloat(pokemon.BaseAtk);
            final Float baseDef = Float.parseFloat(pokemon.BaseDEF);
            Float baseSpAtk = Float.parseFloat(pokemon.BaseSpAtk);
            Float baseSpDef = Float.parseFloat(pokemon.BaseSpDef);
            Float baseSpd = Float.parseFloat(pokemon.BaseSpd);


            barChart = (BarChart) findViewById(R.id.bargraph);
            ArrayList<BarEntry> barEntries = new ArrayList<>();


            //Need to be floats.
            //Y entries
            barEntries.add(new BarEntry(baseHp, 0));
            barEntries.add(new BarEntry(baseAtk, 1));
            barEntries.add(new BarEntry(baseDef, 2));
            barEntries.add(new BarEntry(baseSpAtk, 3));
            barEntries.add(new BarEntry(baseSpDef, 4));
            barEntries.add(new BarEntry(baseSpd, 5));
            BarDataSet barDataSet = new BarDataSet(barEntries, "Pokemon stats");

            //X axis
            ArrayList<String> components = new ArrayList<>();
            components.add("HP");
            components.add("ATK");
            components.add("DEF");
            components.add("SPATK");
            components.add("SPDEF");
            components.add("SPD");

            BarData theData = new BarData(components, barDataSet);
            barChart.setData(theData);
            barChart.setTouchEnabled(true);
            barChart.setDragEnabled(true);
            barChart.setScaleEnabled(true);

            final String TAG = "";

            b1=(Button)findViewById(R.id.button);
            t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if(status != TextToSpeech.ERROR) {
                        t1.setLanguage(Locale.UK);
                    }
                }
            });

            final String typeOne = type1;
            final String typeTwo = type2;

            b1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String toSpeak = "This is a first generation Pokemon named" + pokemon.Name + ". It has the types of "
                            +typeOne + "and" + typeTwo
                            + ". Its health point number is " + baseHp
                            + ". Its attack strength number is " + baseAtk
                            + ". Its defense number is " + baseDef
                            + ". Consider using a Pokemon with high compatability to " + typeOne
                            + " to fight this Pokemon.";
                    Toast.makeText(getApplicationContext(), toSpeak,Toast.LENGTH_SHORT).show();
                    t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                }
            });






            String URL = "http://pokeapi.co.nf/page1.php?DexID=" + pokemon.DexID;
            StringRequest stringRequest = new StringRequest(Request.Method.GET,
                    URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d(TAG, response);

                            ArrayList<PokeMove> pokemoves = new JsonConverter<PokeMove>().toArrayList(response, PokeMove.class);
                            ArrayList<String> pokefinalAray = new ArrayList<String>();


                            for (PokeMove pm : pokemoves) {
                                String MoveName = pm.MoveName;
                                String level = pm.Level;

                                String finalMove = "Move: " + MoveName + "  Learned at: " + level;
                                pokefinalAray.add(finalMove);
                                //System.out.println("harambe");
                            }


                            lv_arr = pokefinalAray.toArray(new String[pokefinalAray.size()]);

                            lv.setAdapter(new ArrayAdapter<String>(PokemonProfile.this, android.R.layout.simple_list_item_1, lv_arr));


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error != null) {
                                Toast.makeText(getApplicationContext(), "something went wrong", Toast.LENGTH_LONG).show();
                                error.printStackTrace();
                            }
                        }
                    });

            MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);


            //Evolutions
            String URL1 = "http://pokeapi.co.nf/page4.php?DexID=" + pokemon.DexID;
            StringRequest stringRequest1 = new StringRequest(Request.Method.GET,
                    URL1,
                    new Response.Listener<String>() {
                        public static final int MY_SOCKET_TIMEOUT_MS = 5000;

                        @Override
                        public void onResponse(String response1) {
                            Log.d(TAG, response1);

                            ArrayList<PokeEvolution> pokeEvolutions = new JsonConverter<PokeEvolution>().toArrayList(response1, PokeEvolution.class);

                            Collections.sort(pokeEvolutions, new Comparator<PokeEvolution>() {
                                @Override
                                public int compare(PokeEvolution p1, PokeEvolution p2) {
                                    return p1.DexID - p2.DexID;
                                }

                            });

                            for(PokeEvolution pee: pokeEvolutions) {
                                System.out.println(pee.getDexID());
                            }

                            final ArrayList<PokeEvolution> pokeEvolutions1 = pokeEvolutions;
                            String url = "http://pokeapi.co.nf/page3.php";
                            StringRequest stringRequest = new StringRequest(Request.Method.GET,
                                    url,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response2) {
                                            Log.d(TAG, response2);
                                            ArrayList<PokeListItem> pokeList = new JsonConverter<PokeListItem>().toArrayList(response2, PokeListItem.class);
                                            ArrayList<PokeListItem> pokeList1 = new ArrayList<>();


                                            for(PokeEvolution pe: pokeEvolutions1) {
                                                int s = pe.DexID;

                                                for(PokeListItem pli: pokeList){
                                                    if(pli.DexID == s){
                                                        pokeList1.add(pli);
                                                    }
                                                }
                                            }


                                            final ProductAdapter adapter = new ProductAdapter(getApplicationContext(), pokeList1);
                                            rvItem1.setAdapter(adapter);


                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            if (error != null) {
                                                Toast.makeText(getApplicationContext(), "something went wrong", Toast.LENGTH_LONG).show();
                                                error.printStackTrace();
                                            }
                                        }
                                    });
                            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                                    MY_SOCKET_TIMEOUT_MS,
                                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);




                            /*final ProductAdapter adapter = new ProductAdapter(getApplicationContext(), pokeEvolutions);
                            rvItem.setAdapter(adapter);

                            sv.setOnQueryTextListener(new SearchView.OnQueryTextListener(){
                            */


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error != null) {
                                Toast.makeText(getApplicationContext(), "something went wrong", Toast.LENGTH_LONG).show();
                                error.printStackTrace();
                            }
                        }
                    });

            MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest1);


        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "PokemonProfile Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.dooba.pokedex/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "PokemonProfile Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.dooba.pokedex/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }





    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }
}

