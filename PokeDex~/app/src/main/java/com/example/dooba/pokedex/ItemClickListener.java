package com.example.dooba.pokedex;

import android.view.View;

/**
 * Created by Dooba on 10/1/2016.
 */
public interface ItemClickListener {
    void onItemClick(View v, int position);
}
