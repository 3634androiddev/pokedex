package com.example.dooba.pokedex;

/**
 * Created by Dooba on 10/1/2016.
 */
public class PokeEvolution {
    public int DexID;
    public String method;

    public void setDexID(int DexID) {
        this.DexID = DexID;
    }
    public int getDexID() {
        return DexID;
    }
    public void setMethod(String method) {
        this.method = method;
    }
}
