package com.example.dooba.pokedex;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.ActionBar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kosalgeek.android.json.JsonConverter;

import java.util.ArrayList;

public class PokeList2 extends AppCompatActivity {
    final String TAG = "";
    RecyclerView rvItem;

    DrawerLayout mDrawerLayout;

    //SearchView
    SearchView sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pokemon_list);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout2);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(false);


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout2);
        NavigationView n = (NavigationView) findViewById(R.id.navigationView2);
        n.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    ////.......

                }
                mDrawerLayout.closeDrawers();  // CLOSE DRAWER
                return true;
            }
        });

        sv= (SearchView) findViewById(R.id.mSearch);
        rvItem = (RecyclerView) findViewById(R.id.my_rec);
        rvItem.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(this);

        rvItem.setLayoutManager(manager);

        String url = "http://pokeapi.co.nf/page3.php";
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);

                        ArrayList<PokeListItem> pokeList = new JsonConverter<PokeListItem>().toArrayList(response, PokeListItem.class);

                        final ProductAdapter adapter = new ProductAdapter(getApplicationContext(), pokeList);
                        rvItem.setAdapter(adapter);

                        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener(){

                            @Override
                            public boolean onQueryTextSubmit(String query) {
                                return false;
                            }

                            @Override
                            public boolean onQueryTextChange(String query) {
                                //filter as you type
                                adapter.getFilter().filter(query);


                                return false;
                            }
                        });
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error != null){
                            Toast.makeText(getApplicationContext(), "something went wrong", Toast.LENGTH_LONG).show();
                            error.printStackTrace();
                        }
                    }
                });

        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);



    }


}
