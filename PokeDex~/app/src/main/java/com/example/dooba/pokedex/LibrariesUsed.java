package com.example.dooba.pokedex;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class LibrariesUsed extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    ActionBarDrawerToggle toggle;
    ListView lv;

    ArrayList<String> listItems=new ArrayList<String>();
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_libraries);

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(this);

        lv = (ListView) findViewById(R.id.listt);

        String first = "Picasso by Square";
        String second = "MpAndroidChart by PhilJay";
        String third = "JSONConverter by Kosalgeek";
        String fourth = "Volley";
        String fifth = "//API pokeapi.co.nf by me (Jonathan)";
        String sixth = "Androids libraries";

        listItems.add(first);
        listItems.add(second);
        listItems.add(third);
        listItems.add(fourth);
        listItems.add(fifth);
        listItems.add(sixth);

        adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItems);
        lv.setAdapter(adapter);




    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(toggle.onOptionsItemSelected(item))
            return(true);
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_menu_1:
                Intent myIntent = new Intent(LibrariesUsed.this, PokeList2.class);
                LibrariesUsed.this.startActivity(myIntent);
                break;
            case R.id.nav_menu_2:
                Intent myIntent2 = new Intent(LibrariesUsed.this, CreditsPage.class);
                LibrariesUsed.this.startActivity(myIntent2);
                break;
            case R.id.nav_menu_3:

                break;
        }




        return false;
    }
}
