package com.example.dooba.pokedex;

import android.widget.Filter;

import java.util.ArrayList;

/**
 * Created by Dooba on 10/1/2016.
 */
public class CustomFilter extends Filter {

    ProductAdapter adapter;
    ArrayList<PokeListItem> filterList;

    public CustomFilter(ArrayList<PokeListItem> filterList, ProductAdapter adapter){
        this.adapter = adapter;
        this.filterList = filterList;
    }


    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        //check validity

        if(constraint != null && constraint.length() > 0){
            //change to uppercase
            constraint = constraint.toString().toUpperCase();


            //store filtered pokemon
            ArrayList<PokeListItem> filteredPokemon = new ArrayList<>();

            for(int i = 0; i < filterList.size(); i++){
                //check
                if(filterList.get(i).Name.toUpperCase().contains(constraint)){
                    filteredPokemon.add(filterList.get(i));
                }
            }
            results.count=filteredPokemon.size();
            results.values=filteredPokemon;
        } else {
            results.count=filterList.size();
            results.values=filterList;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.pokemon=  (ArrayList<PokeListItem>) results.values;
        //refresh
        adapter.notifyDataSetChanged();

    }
}
