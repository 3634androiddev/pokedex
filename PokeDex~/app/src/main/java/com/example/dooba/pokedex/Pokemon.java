package com.example.dooba.pokedex;

import java.io.Serializable;

/**
 * Created by Dooba on 10/1/2016.
 */
public class Pokemon implements Serializable {

    public String Name;
    public int DexID;
    public String Type;
    public String BaseHp;
    public String BaseAtk;
    public String BaseDEF;
    public String BaseSpAtk;
    public String BaseSpDef;
    public String BaseSpd;
}
