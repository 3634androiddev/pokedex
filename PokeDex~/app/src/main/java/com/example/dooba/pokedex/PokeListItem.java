package com.example.dooba.pokedex;

import java.io.Serializable;

/**
 * Created by Dooba on 9/29/2016.
 */
public class PokeListItem implements Serializable {
    public int DexID;
    public String Name;
    public String Type;

}
