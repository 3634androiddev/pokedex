package com.example.dooba.pokedex;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kosalgeek.android.json.JsonConverter;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


/**
 * Created by Dooba on 8/30/2016.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> implements Filterable {

    Context context;
    ArrayList<PokeListItem> pokemon, filterList;
    CustomFilter filter;
    RequestQueue rq;


    public ProductAdapter(Context context, ArrayList<PokeListItem> pokemon) {
        this.context = context;
        this.pokemon = pokemon;
        this.filterList = pokemon;
    }

    View view;

    @Override
    public ProductAdapter.ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.from(parent.getContext()).inflate(R.layout.row, parent, false);

        ProductViewHolder productViewHolder = new ProductViewHolder(view);
        return productViewHolder;
    }

    @Override
    public void onBindViewHolder(ProductAdapter.ProductViewHolder holder, int position) {

        final PokeListItem pokemonCur = pokemon.get(position);
        holder.tvName.setText(pokemonCur.Name);
        holder.tvPrice.setText(pokemonCur.Type);

        OkHttpClient myOkHttpClient = new OkHttpClient();
        myOkHttpClient.setConnectTimeout(15, TimeUnit.SECONDS);

        Uri uri = Uri.parse("android.resource://com.example.dooba.pokedex/drawable/imglittle"+pokemonCur.DexID);

        Picasso picasso = new Picasso.Builder(context)
                .downloader(new OkHttpDownloader(myOkHttpClient))
                .build();


        picasso
                .with(context)
                .load(uri)
                .into(holder.ivImg);


        rq = Volley.newRequestQueue(context);
        holder.ivImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String TAG = "";

                String URL = "http://pokeapi.co.nf/page2.php?DexID="+pokemonCur.DexID;
                StringRequest stringRequest = new StringRequest(Request.Method.GET,
                        URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d(TAG, response);

                                ArrayList<Pokemon> pokemon = new JsonConverter<Pokemon>().toArrayList(response, Pokemon.class);
                                Pokemon currentPokemon = pokemon.get(0);



                                Intent in = new Intent(context, PokemonProfile.class);
                                in.putExtra("pokemon", currentPokemon);
                                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(in);

                                Toast.makeText(context, currentPokemon.Name, Toast.LENGTH_LONG).show();

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if(error != null){
                                    Toast.makeText(context, "something went wrong", Toast.LENGTH_LONG).show();
                                    error.printStackTrace();
                                }
                            }
                        });

                MySingleton.getInstance(context).addToRequestQueue(stringRequest);



            }
        });

        //implement click listener
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Snackbar.make(v, pokemon.get(position).Name, Snackbar.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public int getItemCount() {
        if (pokemon != null) {
            return pokemon.size();
        } else {
            return 0;
        }
    }

    @Override
    public Filter getFilter() {

        if (filter == null) {
            filter = new CustomFilter(filterList, this);
        }

        return filter;
    }


    //view holder class
    public static class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView ivImg;
        public TextView tvPrice;
        public TextView tvName;

        ItemClickListener itemClickListener;

        public ProductViewHolder(View itemView) {
            super(itemView);
            ivImg = (ImageView) itemView.findViewById(R.id.ivImg);
            tvName = (TextView) itemView.findViewById(R.id.pokeName);
            tvPrice = (TextView) itemView.findViewById(R.id.pokeType);
        }

        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(v, getLayoutPosition());
        }

        public void setItemClickListener(ItemClickListener ic) {
            this.itemClickListener = ic;
        }
    }



}
